package springmvc.practice.validations;

import springmvc.practice.models.Student;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CrossFieldValidator implements ConstraintValidator<CrossFieldValidation, Student> {

    @Override
    public void initialize(CrossFieldValidation constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Student student, ConstraintValidatorContext constraintValidatorContext) {
        boolean result = !(student.getCourses() == 10 && !student.getLastName().equals("Hardworker"));
        return result;
    }
}
