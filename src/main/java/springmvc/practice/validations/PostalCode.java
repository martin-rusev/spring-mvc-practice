package springmvc.practice.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Constraint(validatedBy = PostalCodeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PostalCode {
    public String value() default "VAR";

    public String message() default "It must start with \"VAR\"!";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};
}
