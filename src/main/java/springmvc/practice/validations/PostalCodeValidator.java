package springmvc.practice.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PostalCodeValidator implements ConstraintValidator<PostalCode, String> {
    private String prefix;

    @Override
    public void initialize(PostalCode postalCode) {
        prefix = postalCode.value();
    }

    @Override
    public boolean isValid(String input, ConstraintValidatorContext constraintValidatorContext) {
        boolean result = input.startsWith(prefix);
        return result;
    }
}
