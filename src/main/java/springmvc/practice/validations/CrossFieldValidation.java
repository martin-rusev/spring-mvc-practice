package springmvc.practice.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CrossFieldValidator.class)
public @interface CrossFieldValidation {
    public String message() default "If you choose 10 courses your lastname should be \"Hardworker\"!";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};
}

