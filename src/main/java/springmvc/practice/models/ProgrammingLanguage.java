package springmvc.practice.models;

public enum ProgrammingLanguage {
    JAVA,
    JavaScript,
    Ruby,
}
