package springmvc.practice.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import springmvc.practice.validations.CrossFieldValidation;
import springmvc.practice.validations.PostalCode;

import javax.validation.constraints.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
@CrossFieldValidation
public class Student {
    @NotBlank(message = "First name should not be blank!")
    private String firstName;

    @NotBlank(message = "Last name should not be blank!")
    @Size(max = 10, message = "Last name should not have more then 10 characters!")
    private String lastName;

    @Min(value = 1, message = "You should specify at least one course!")
    @Max(value = 10, message = "You can't specify more then ten courses!")
    @NotNull(message = "The number of courses should not be left blank!")
    private Integer courses;

    @Pattern(regexp = "[a-zA-Z0-9]{4}", message = "Postal code must be 4 characters and should not contain any special symbols!")
    @PostalCode
    private String postalCode;

    private ProgrammingLanguage language;
    private Set<String> operatingSystem = new HashSet<>();

}
