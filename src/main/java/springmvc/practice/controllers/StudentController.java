package springmvc.practice.controllers;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import springmvc.practice.models.ProgrammingLanguage;
import springmvc.practice.models.Student;

import javax.validation.Valid;

@Controller
@RequestMapping("/students")
public class StudentController {

    @GetMapping
    public String showForm(Model model) {

        model.addAttribute("student", new Student());
        model.addAttribute("allLanguages", ProgrammingLanguage.values());

        return "new-form";
    }

    @PostMapping
    public String register(@Valid @ModelAttribute("student") Student student,
                           BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "new-form";
        }
        return "student-confirmation";
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(false);
        webDataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }
}
