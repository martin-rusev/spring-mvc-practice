package springmvc.practice.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {

    @GetMapping("/")
    public ModelAndView home() {
        String view = "index";

        Map<String, Object> model = new HashMap<>();

        model.put("name", "John Doe");

        return new ModelAndView(view, model);
    }

    @GetMapping("/hello")
    public ModelAndView hello() {
        String view = "hello";

        Map<String, Object> model = new HashMap<>();

        model.put("date", LocalDateTime.now());

        return new ModelAndView(view, model);
    }

    @GetMapping("/redirect")
    public String hello2() {
        return "redirect";
    }
}
