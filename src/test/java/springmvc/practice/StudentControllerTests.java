package springmvc.practice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest
public class StudentControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void showStudentForm() throws Exception {
        mockMvc.perform(get("/students"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("new-form"))
                .andExpect(model().size(2))
                .andExpect(model().attributeExists("student"));
    }

    @Test
    public void showStudentForm_return_form_ifErrors() throws Exception {
        mockMvc.perform(post("/students")
                .param("lastName", "Doe")
                .param("courses", "5")
                .param("postalCode", "False"))
                .andExpect(view().name("new-form"));
    }

    @Test
    public void testSubmitWatchlistItemForm() throws Exception {
        mockMvc.perform(post("/students")
                .param("firstName", "John")
                .param("lastName", "Doe")
                .param("courses", "5")
                .param("postalCode", "VAR4"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("student-confirmation"));
    }
}
